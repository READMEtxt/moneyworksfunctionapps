using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System.Threading;
using System.Net;
using System.Xml;

namespace mwpollingbyordernumber
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("trigger function processed request");

            string mwordernumber = req.Query["mwordernumber"];
            string firstname = req.Query["firstname"];
            string lastname = req.Query["lastname"];


            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            mwordernumber = mwordernumber ?? data?.mwordernumber;
            firstname = firstname ?? data?.firstname;
            lastname = lastname ?? data?.lastname;

            string mwurl = "https://za01.moneyworks.net.nz:6710/REST/AGWKS%2FThe_Mattress_Warehouse%2FQA+The+Mattress+Warehouse.moneyworks/export/table=transaction&format=xml&limit=10&search=tofrom%3D%22" + firstname + "%20" + lastname + "%22%20and%20namecode%3D%22" + mwordernumber + "@%22";

            //var client = new RestClient("https://za01.moneyworks.net.nz:6710/REST/AGWKS%2FThe_Mattress_Warehouse%2FQA+The+Mattress+Warehouse.moneyworks/export/table=transaction&format=xml&limit=10&search=namecode%3D%22" + mwordernumber + "%22");
            var client = new RestClient(mwurl);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Basic QUdXS1MvVGhlX01hdHRyZXNzX1dhcmVob3VzZTpEYXRhY2VudHJlOlBBU1NXT1JE, Basic R3JhaGFtOkRvY3VtZW50OllfVElGSVk3T01XNVFY");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            Console.WriteLine(mwurl);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(response.Content);
            string jsonconvertedxml = JsonConvert.SerializeXmlNode(xmldoc);

            //string responseMessage = string.IsNullOrEmpty(mwordernumber)
            //    ? "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response."
            //    : $"Hello, {mwordernumber}. This HTTP triggered function executed successfully.";

            return new OkObjectResult(jsonconvertedxml);
        }
    }
}
